const pn532 = require('pn532')
const SerialPort = require('serialport')

const serialPort = new SerialPort('/dev/serial0', { baudRate: 115200 })
const rfid = new pn532.PN532(serialPort)
const ndef = require('ndef')

const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://hassbian.local', {username: 'nespinfc', password: 'nespinfc'})

function describeWKT (record) {
  switch (record.type) {
    case ndef.RTD_TEXT:
      record.recordType = 'RTD_TEXT'
      break
    case ndef.RTD_URI:
      record.recordType = 'RTD_URI'
      break
    case ndef.RTD_SMART_POSTER:
      record.recordType = 'RTD_SMART_POSTER'
      if (!record.value) {
        var records = ndef.decodeMessage(record.payload)
        record.value = describeRecords(records)
      }
      break
  }
  if (record.value) {
    delete record.payload
  }
  return record
}

function describeRecords (records) {
  return records.map((record) => {
    record.typeNameFormat = 'TNF_WELL_KNOWN'
    switch (record.tnf) {
      case ndef.TNF_WELL_KNOWN:
        return describeWKT(record)
      case ndef.TNF_EMPTY:
        break
      default:
        return record
    }
  })
}

function onMessage (topic, message) {
  // message is Buffer
  console.log('message', message.toString())
}

var lastUid = ''

rfid.on('ready', function () {
  console.log('rfid ready')
  client.on('connect', function () {
    console.log('mqtt connected')
    client.on('message', onMessage)

    rfid.on('tag', function (tag) {
      // Prevent repeatedly spamming a tag that is sitting on the reader
      if (lastUid === tag.uid) {
        return
      }
      lastUid = tag.uid

      // Save ATQA as string for serialization
      tag.ATQA = tag.ATQA.readUInt16BE(0).toString(0x10)

      rfid.readNdefData().then(function (data) {
        var records = ndef.decodeMessage(Array.from(data))
        tag.ndef = describeRecords(records)

        client.publish('tag', JSON.stringify(tag))
      })
    })
  })
})
