const pn532 = require('pn532')
const SerialPort = require('serialport')

const serialPort = new SerialPort('/dev/serial0', { baudRate: 115200 })
const rfid = new pn532.PN532(serialPort)
const ndef = require('ndef')
const Blink1 = require('node-blink1');


var blink1 = (function blinkAvailable() {
  try {
    return new Blink1();
  } catch (error) {}
  return false
})()


console.log('Waiting for rfid ready event...')
rfid.on('ready', function () {
  var messages = [
    ndef.uriRecord('invision-studio://')
  ]
  var data = ndef.encodeMessage(messages)

  console.log('Waiting for a tag...')
  blink1 && blink1.setRGB(0x00, 0x00, 0xff)

  rfid.scanTag().then(function (tag) {
    console.log('Tag found:', tag)
    blink1 && blink1.setRGB(0x00, 0xff, 0xff)

    console.log('Writing tag data...')
    rfid.writeNdefData(data).then(function (response) {
      console.log('Write successful')
      blink1 && blink1.setRGB(0x00, 0xff, 0x00, () => {
        setTimeout(() => blink1 && blink1.off(), 3000)
      })
    })
  })
})
